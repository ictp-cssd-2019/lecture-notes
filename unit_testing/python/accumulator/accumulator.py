class Accumulator(object):
	'''\
	An accumulator that keeps track of the sum of numbers added to it
	as well as the last number that was added.
	'''
	def __init__(self, initial_value):
		super(Accumulator, self).__init__()

		# The sum of the numbers added so far
		self.sum = initial_value

		# The last number that was added
		self.last = initial_value



	def add(self, number):
		self.sum += number
