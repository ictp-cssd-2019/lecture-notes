import sys
sys.path.append('..') # add the directory above to the PYTHONPATH, so that Python will find our module "accumulator"!
import accumulator

def test_accumulator_constructor():
	'Test that an Accumulator object is constructed as desired'
	a = accumulator.Accumulator(4.)
	assert a.sum == 4.
	assert a.last == 4.

def test_accumulator_add():
	'Test that adding to an Accumulator object works'
	a = accumulator.Accumulator(4.)
	a.add(12.)
	assert a.sum == 16.
	assert a.last == 12.
