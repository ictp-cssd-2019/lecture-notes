# Unit tests in Python using Pytest or Nose
Here are instructions on how you can run the examples and implement your own tests.

## Running some dummy tests

Go into the directory `standalone` and run either

```bash
pytest
```

or

```bash
nosetests
```

Actually, try both! Spot any differences with how the two test frameworks print output? Add the `--help` argument to the program calls to see what options you have!


## Testing a module

Go into the directory you're interested in (`accumulator`) and take a look at the very simple class implemented in `accumulator.py`. Go into the `test` directory and run the tests as above.

Can you see why some tests fail? Can you fix the `Accumulator` class to work as specified with the help of the tests?

Try adding your own tests as well!


## Measuring code coverage

##### Pytest

Install Pytest-cov (`pip3 install --user pytest-cov`). Now you may add the following arguments to your Pytest call:

```bash
pytest --cov                # line coverage
pytest --cov --cov-branch   # branch coverage
```

##### Nose

For Nose, the call might look like this:

```bash
nosetests --with-coverage --cover-package=.
```

What happens if you remove the `--cover-package` argument, or set it to e.g. `--cover-package=numpy.version`?

Note: I sometimes need to add the argument `--cover-erase` to avoid errors or taking into account packages that shouldn't be included due to my `--cover-package` argument. I don't know why, and have not investigated, because I usually use Pytest rather than Nose.
