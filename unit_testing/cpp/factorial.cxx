#include "factorial.h"

unsigned factorial(unsigned number) {
	if (number <= 1) return number;
	return factorial(number - 1) * number;
}
