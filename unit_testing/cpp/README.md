# Unit tests in C++ using the Catch testing framework

Here are instructions on how you can run the examples and implement your own tests.

### Step 1: download Catch

Catch is a very light-weight testing framework that is provided as a single C++ header file. Download it into this directory by running:

```bash
wget https://github.com/catchorg/Catch2/releases/download/v2.7.2/catch.hpp
```

The file `catch.hpp` should now be in your directory.

[Catch is licensed under the Boost license](https://github.com/catchorg/Catch2/blob/master/LICENSE.txt), which is very permissive, so you can just download the header and include it in your software project.

### Step 2: play with Catch by doing some ad-hoc tests

For now, we don't test any actually usable software, we just write a few standalone tests and run them Catch. The goal is to familiarise ourselves with the syntax of a Catch test and how to compile and run it.

Make a new file `test_standalone.cxx` and enter the following content:

```cpp
#define CATCH_CONFIG_MAIN
// ^ this tells Catch to provide a main() function.
// Only do this in one cxx file entering the executable.
#include "catch.hpp"

#include <stdexcept> // needed to get std::logic_error etc.
#include <vector>

TEST_CASE("Check basic arithmetic functions") {
	CHECK( 4 + 1 == 5 );
	CHECK( 2 * 3 == 6 );
	CHECK( 2 * 3 == 7 );
}

TEST_CASE("Check some exception throwing") {
	CHECK_THROWS( throw std::logic_error("Your logic is flawed!") );
	CHECK_THROWS_AS( throw std::logic_error("Your logic is flawed!"), std::logic_error );
	CHECK_THROWS_AS( throw std::logic_error("Your logic is flawed!"), std::runtime_error );
}

TEST_CASE("Test subtraction of floating point numbers") {
	CHECK( 1.2 - 1.0 == 0.2 );
	CHECK( 1.2 - 1.0 == Approx(0.2) );
	CHECK( 1.2 - 1.0 == Approx(0.2).epsilon(0.01) );
	CHECK( 1.2 - 1.0 == Approx(0.2).epsilon(0.0000000000000000001) );
}

TEST_CASE("Test std::vector size and element access") {
	std::vector<double> v = {42.0, 1.3};

	CHECK( v.size() == 2 );
	CHECK( v.at(0) == 42.0 );
	CHECK_THROWS_AS( v.at(2), std::out_of_range );
}
```

Compile the test program using

```bash
g++ test_standalone.cxx -o test_standalone
```

If you get a lot of errors, this may be because your compiler uses a too old version of the C++ standard by default. Catch (version 2) needs at least C++11, so retry compiling with:

```bash
g++ test_standalone.cxx -o test_standalone -std=c++11
```

Run the test program as

```bash
./test_standalone
```

Note that `./test_standalone -?` prints the additional command line options that you could specify (can be very useful!).

If you replace the word `CHECK` in the above example with `REQUIRE`, the execution of a test case will terminate as soon as one assertion fails. With `CHECK`, all the assertions are run. Which of the two you should use depends on your use case!

### Step 3: take a look at the example software and compile it

Take a look at the C++ source files `factorial.h` and `factorial.cxx`. They just implement a simple function for calculating the factorial of a given number. We want to test that this function works properly.

Let's compile `factorial.cxx` into a shared (= dynamic) library:

```bash
# compile source file:
gcc -c -fPIC factorial.cxx -o factorial.o
# make shared library from compiled file:
gcc -shared factorial.o -o libfactorial.so
```

(Note for Mac users: you need to call your library `libfactorial.dylib` instead of `libfactorial.so`!)

Now create a test program for the library, called `test_factorial.cxx` and put the following code into it:

```cpp
#define CATCH_CONFIG_MAIN
// ^ this tells Catch to provide a main() function.
// Only do this in one cxx file entering the executable.
#include "catch.hpp"

#include "factorial.h"

TEST_CASE("Factorial calculation for numbers > 0") {
    REQUIRE( factorial(1) == 1 );
    REQUIRE( factorial(2) == 2 );
    REQUIRE( factorial(3) == 6 );
    REQUIRE( factorial(10) == 3628800 );
}

TEST_CASE("Factorial of zero") {
	// The factorial of zero is defined as 1
	REQUIRE( factorial(0) == 1 );
}
```

To compile it, do:

```bash
g++ test_factorial.cxx -o test_factorial -L. -lfactorial
```

Note that we needed to link our factorial library to the executable! Otherwise the test program doesn't know how to run the `factorial()` function (in fact, we get a linking-time error).

Run the tests by doing:

```bash
./test_factorial
```

Can you fix the factorial function so that all tests pass?