#define CATCH_CONFIG_MAIN
// ^ this tells Catch to provide a main() function.
// Only do this in one cxx file entering the executable.
#include "catch.hpp"

#include "factorial.h"

TEST_CASE("Factorial calculation for numbers > 0") {
    REQUIRE( factorial(1) == 1 );
    REQUIRE( factorial(2) == 2 );
    REQUIRE( factorial(3) == 6 );
    REQUIRE( factorial(10) == 3628800 );
}

TEST_CASE("Factorial of zero") {
	// The factorial of zero is defined as 1
	REQUIRE( factorial(0) == 1 );
}
