#define CATCH_CONFIG_MAIN
// ^ this tells Catch to provide a main() function.
// Only do this in one cxx file entering the executable.
#include "catch.hpp"

#include <stdexcept> // needed to get std::logic_error etc.
#include <vector>

TEST_CASE("Check basic arithmetic functions") {
	CHECK( 4 + 1 == 5 );
	CHECK( 2 * 3 == 6 );
	CHECK( 2 * 3 == 7 );
}

TEST_CASE("Check some exception throwing") {
	CHECK_THROWS( throw std::logic_error("Your logic is flawed!") );
	CHECK_THROWS_AS( throw std::logic_error("Your logic is flawed!"), std::logic_error );
	CHECK_THROWS_AS( throw std::logic_error("Your logic is flawed!"), std::runtime_error );
}

TEST_CASE("Test subtraction of floating point numbers") {
	CHECK( 1.2 - 1.0 == 0.2 );
	CHECK( 1.2 - 1.0 == Approx(0.2) );
	CHECK( 1.2 - 1.0 == Approx(0.2).epsilon(0.01) );
	CHECK( 1.2 - 1.0 == Approx(0.2).epsilon(0.0000000000000000001) );
}

TEST_CASE("Test std::vector size and element access") {
	std::vector<double> v = {42.0, 1.3};

	CHECK( v.size() == 2 );
	CHECK( v.at(0) == 42.0 );
	CHECK_THROWS_AS( v.at(2), std::out_of_range );
}
