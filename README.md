# ICTP 2019 - Collaborative Scientific Software Development

[Course website](http://indico.ictp.it/event/8654/)

[Etherpad](https://etherpad.net/p/ictp2019)

[Locally saved snapshot of the etherpad](ictp2019.txt)

## Day 1

- [Course introduction](IG-Workshop-Intro.pdf)
- [Python intro](Python-1.pdf)
- Project Euler exercises


## Day 2

- [Compiled languages](CompilingAndLinking.pdf) (Lecture notes include examples for [practicing compiling](compiling_examples.tar.gz) as well as a [full collection](multi-language-ref.tar.gz) of examples about to compile mixed languages C/Fortran, C/C++, etc...)
- [Numpy, Pandas](python_for_scientific_computing.pdf)
- [Documentation: part 1](Documentation_part1.pdf)
- [Land or Water exercise](http://users.ictp.it/~dgrells/cssd2019/landcover/)

## Day 3
- [Earthquake localization exercise in pandas](Ex_pandas.ipynb)
- [OO Design](OO-Design.pdf)
- [How Python does OO](Python_Classes.pdf)
- [Train exercise example output](train-answers.tgz)

## Day 4
- [Optimization](Optimization.pdf)
- [Floating Point Arithmetic](Corbetta_FP_slides.pdf)
- [Floating Point Arithmetic Exercise](https://gitlab.com/acorbe/SMR3199_FP_ex)


## Day 5
- [Git intro](Git_tutorial.pdf)
- [Plotting and animation examples](plotting)
- [Debugging and Profiling](Debugging_profiling.pdf)

## Day 6

- [Git for groups](slide_AC_version_control_git_groups.pdf)

## Day 7

- [Conversational development](conversational_development_corbetta.pdf)
- [Documentation Prat 2](Documentation.pdf)

## Day 8
- [Makefile](Makefile.pdf)
- [Containers, CI, CD](docker-CI-CD-corbetta.pdf)
- Task Farming [Slides](TaskFarming.pdf) and [Examples](task_farming_example.tar.gz)

### Presentation Videos
- https://drive.google.com/drive/folders/1Oi_6hWEADSWJGdla_8IlTkh2Ki7TG6Bi

### Plotting links
- https://matplotlib.org/
- https://seaborn.github.io/
- http://holoviews.org/
- http://scitools.org.uk/cartopy/docs/latest

### Copyright and licenses
- https://choosealicense.com/
- http://www.gnu.org/licenses/license-list.html
- http://www.gnu.org/licenses/quick-guide-gplv3
- https://creativecommons.org/share-your-work/licensing-considerations/


