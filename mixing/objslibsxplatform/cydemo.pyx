#cython: embedsignature=True, c_string_type=str, c_string_encoding=utf8

cimport cydemo as c

def cfib(a):
    "C++ implementation of a'th Fibonacci number"
    return c.fib(a)

def ccollaz_len(a):
    "C++ implementation of a'th Collaz sequence length"
    return c.collaz_len(a)

def cdbldbl(a):
    "C++ implementation of doubling a double"
    return c.dbldbl(a)
