static const int CVAR1 = 3;
const int CVAR2 = 1;
static int CVAR3 = 4;
int CVAR4 = 2;

struct Bar {
  double x;
};


double dbldbl(double x) {
  return 2*x;
}


unsigned int fib(unsigned int n) {
  if (n == 1 || n == 2) return 1;
  unsigned int a = 1, b = 1, tmp = 0;
  for (int i = 3; i <= n; ++i) {
    tmp = b;
    b += a;
    a = tmp;
  }
  return b;
}


unsigned int collaz_len(unsigned int n) {
  if (n == 0) return 0;
  unsigned int count = 1;
  while (n != 1) {
    n = (n % 2 == 0) ? n/2 : (3*n + 1);
    count += 1;
  }
  return count;
}
