SUBROUTINE FIB(I)
  I1 = 1; I2 = 1
  DO J = 3, I
     I3 = I1 + I2
     I2 = I1
     I1 = I3
  END DO
  I = I1
  RETURN
END SUBROUTINE FIB


SUBROUTINE COLLAZ_LEN(I)
  N = 1
  DO WHILE (I /= 1)
     IF (MOD(I,2) == 0) THEN
        I = I/2
     ELSE
        I = 3*I + 1
     END IF
     N = N + 1
  END DO
  I = N
END SUBROUTINE COLLAZ_LEN


! PROGRAM X
!   DO I = 1, 10
!      K = I
!      CALL FIB(K)
!      PRINT *,I,'th Fibonacci number is',K
!   END DO
!   DO I = 1, 10
!      K = I
!      CALL COLLAZ_LEN(K)
!      PRINT *,I,'th Collaz seq length is',K
!   END DO
!   STOP
! END PROGRAM X
