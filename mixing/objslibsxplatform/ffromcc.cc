#include <iostream>
#include <cassert>

// Versions from C++ ccdemo.o
unsigned int fib(unsigned int);
unsigned int collaz_len(unsigned int);

// Versions from F90 fdemo.f90
extern "C" {
  void fib_(int*);
  void collaz_len_(int*);
}

int main() {
  for (int i = 1; i < 10; ++i) {
    int f1 = fib(i);
    int f2 = i; fib_(&f2);
    std::cout << f1 << " " << f2 << std::endl;
    assert(f1 == f2);

    int c1 = collaz_len(i);
    int c2 = i; collaz_len_(&c2);
    std::cout << c1 << " " << c2 << std::endl;
    assert(c1 == c2);
  }
  return 0;
}
