// Versions from C++ ccdemo.o
unsigned int fib(unsigned int);
unsigned int collaz_len(unsigned int);

extern "C" {
  void cfib_(int* i) {
    unsigned int tmp = (unsigned int) *i;
    tmp = fib(tmp);
    *i = tmp;
  }
  void ccollaz_len_(int* i) {
    unsigned int tmp = (unsigned int) *i;
    tmp = collaz_len(tmp);
    *i = tmp;
  }
}
