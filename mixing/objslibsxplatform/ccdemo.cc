#include <string>

static const int CCVAR1 = 3;
const int CCVAR2 = 1;
static int CCVAR3 = 4;
int CCVAR4 = 2;


struct Foo {
  Foo() { x = 0; }
  Foo(const std::string&) { x = 1; }
  ~Foo() {}
  double x;
};


double dbldbl(double x) {
  return 2*x;
}


unsigned int fib(unsigned int n) {
  unsigned int a = 1, b = 1;
  for (int i = 3; i <= n; ++i) {
    a += b;
    std::swap(a, b);
  }
  return b;
}


unsigned int collaz_len(unsigned int n) {
  if (n == 0) return 0;
  unsigned int count = 1;
  while (n != 1) {
    n = (n % 2 == 0) ? n/2 : (3*n + 1);
    count += 1;
  }
  return count;
}


// #include <iostream>
// int main() {
//   using namespace std;
//   for (int i = 0; i < 10; ++i)
//     cout << fib(i) << " ";
//   cout << endl;
//   for (int i = 0; i < 10; ++i)
//     cout << collaz_len(i+1) << " ";
//   cout << endl;
//   return 0;
// }
