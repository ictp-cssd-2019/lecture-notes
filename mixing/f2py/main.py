import sum_abs_f2py

import random
import numpy as np

N = 1000000
xlst = [random.uniform(-10.,10.) for _ in range(N)]
xarr = np.array(xlst, dtype='float64')

def f2py_plain(xs):
    return sum_abs_f2py.sum_abs(xs,N)

if __name__ == '__main__':
    print(f2py_plain(xlst))
    print(f2py_plain(xarr))

