! compile this using
! f2py -c -m sum_abs_f2py sum_abs.f90 --opt='-O0'

subroutine sum_abs(in, num, out)
    integer, intent(in) :: num
    real (kind=8), intent(in) :: in(num)
    real (kind=8), intent(out) :: out
    real (kind=8) :: sum
    sum = 0
    do i=1,num
      sum = sum + ABS(in(i))
    end do
    out = sum
end subroutine sum_abs
